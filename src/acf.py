import numpy as np
from numba import jit, njit

@jit(nopython=True)
def get_acf(a, n, normed=True):
    """
    a (array-like) Input array holding the data
    n (int) Length of array holding autocorrelation funtion (acf)
    normed = True (bool): If true, autocorrelation funtion starts at one 
    """
    c=np.zeros(n)
    N=len(a)
    for i in range(n):
        for j in range(0, N-i):
            c[i]+=a[j]*a[j+i]
        c[i]/=float(N-i)
    if normed==True:
        c/=c[0]
    return c
